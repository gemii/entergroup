package com.jl.nutcloud.core.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jl.nutcloud.core.dto.EnterGroupInfoReq;
import com.jl.nutcloud.core.dto.PageProfile;
import com.jl.nutcloud.core.dto.RobotGroupReq;
import com.jl.nutcloud.core.otd.CommonResult;
import com.jl.nutcloud.core.service.IAdminService;

/**
 * ClassName:AdminController.java Function: the interface to administrators Reason: for administrators
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 20, 2017
 *
 * @see
 */
@Controller
@RequestMapping("/noauth/admin")
public class AdminController {

	@Autowired
	private IAdminService adminService;
	
	/**
	 * 
	 * @param userID 用户ID
	 * @param groupName 搜索的群名
	 * @param page 页码包装类
	 * @param response
	 * @return
	 * TODO 根据groupName page 获取群列表
	 */
	@RequestMapping(value = "/groups", method = RequestMethod.GET)
	@ResponseBody
	public CommonResult groupList(@RequestParam String groupName,
			@RequestParam Integer userID,
			PageProfile page,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return adminService.groupList(userID, groupName, page);
	}
	
	/**
	 * 
	 * @param enterInfoReq 请求包装类
	 * @param page 页码包装类
	 * @return
	 * TODO 入群信息管理
	 */
	@RequestMapping(value = "/enterInfo", method = RequestMethod.GET)
	@ResponseBody
	public CommonResult enterGroupInfo(EnterGroupInfoReq enterInfoReq,
			PageProfile page,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return adminService.enterInfo(enterInfoReq, page);
	}

	/**
	 * 
	 * @param robotGroupReq 请求包装类
	 * @param page 页码包装类
	 * @return
	 * TODO 小助手所在群信息
	 */
	@RequestMapping(value = "/robotGroup", method = RequestMethod.GET)
	@ResponseBody
	public CommonResult getRobotGroup(RobotGroupReq robotGroupReq,
			PageProfile page,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return adminService.getRobotGroup(robotGroupReq, page);
	}
	
	/**
	 * 
	 * @param id 入群记录ID
	 * @param status 入群状态
	 * @return
	 * TODO 管理员修改  ID记录的入群状态
	 */
	@RequestMapping(value = "/enterInfo", method = RequestMethod.PUT)
	@ResponseBody
	public CommonResult updateEnterGroupStatus(@RequestParam Integer id,
			@RequestParam String status,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return adminService.updateEnterGroupStatus(id, status);
	}
}
