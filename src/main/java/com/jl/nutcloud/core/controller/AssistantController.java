package com.jl.nutcloud.core.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jl.nutcloud.core.otd.CommonResult;
import com.jl.nutcloud.core.service.IAssistantService;

/**
 * ClassName:AssistantController.java Function: Reason: for assistant
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 20, 2017
 *
 * @see
 */
@Controller
@RequestMapping("/noauth/assistant")
public class AssistantController {

	@Autowired
	private IAssistantService assistantService;
	
	/**
	 * 
	 * @param userID  用户ID
	 * @param robotName 小助手名
	 * @return
	 * TODO 获取此用户的机器人列表
	 */
	@RequestMapping(value = "/robots", method = RequestMethod.GET)
	@ResponseBody
	public CommonResult robotsList(@RequestParam Integer userID,
			@RequestParam String robotName,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return assistantService.robotsList(userID, robotName);
	}
	
	/**
	 * 
	 * @param userID 用户ID
	 * @return
	 * TODO 用户添加小助手
	 */
	@RequestMapping(value = "/robot", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult addRobot(@RequestParam Integer userID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return assistantService.addRobot(userID);
	}
	
	/**
	 * 
	 * @param userID 用户ID
	 * @param tag 机器人标签
	 * @return
	 * TODO 用户重启小助手
	 */
	@RequestMapping(value = "/robot", method = RequestMethod.GET)
	@ResponseBody
	public CommonResult restartRobot(@RequestParam Integer userID,
			@RequestParam String tag,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return assistantService.restartRobot(userID, tag);
	}
	
	/**
	 * 
	 * @param tag 机器人标签
	 * @return
	 * TODO 用户删除小助手
	 */
	@RequestMapping(value = "/delrobot", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult delrobot(@RequestParam String tag,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return assistantService.delRobot(tag);
	}
}
