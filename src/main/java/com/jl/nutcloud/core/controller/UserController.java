package com.jl.nutcloud.core.controller;


import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.jl.nutcloud.core.dto.BabyProfile;
import com.jl.nutcloud.core.dto.UserInfoReq;
import com.jl.nutcloud.core.otd.CommonResult;
import com.jl.nutcloud.core.service.IUserService;


@Controller
@RequestMapping("/noauth/user")
public class UserController {

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private IUserService userService;
	
	/**
	 * 
	 * @param userInfo 用户信息扩展包装类
	 * @param babyInfo 宝宝信息包装类
	 * @return
	 * TODO 用户提交表单
	 */
	@RequestMapping(value = "/info", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult submitForm(UserInfoReq _userInfo,
			BabyProfile _babyInfo,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		try {
			return userService.submitForm(_userInfo, _babyInfo);
		} catch (Exception e) {
			logger.error("_userInfo="+JSONObject.toJSONString(_userInfo));
			logger.error("_babyInfo="+JSONObject.toJSONString(_babyInfo));
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, "服务器繁忙");

		}
	}
	
	/**
	 * 
	 * @param phoneNumber 手机号
	 * @return
	 * TODO 验证手机号时获取验证码
	 */
	@RequestMapping(value = "/phoneCode", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult getPhoneCode(@RequestParam String phoneNumber,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return userService.getPhoneCode(phoneNumber);
	}

	/**
	 * 
	 * @param _robotID 小助手ID
	 * @param _referralCode 用户所填推荐码
	 * @return
	 * TODO 根据用户所填推荐码返回对应的中心名
	 */
	@RequestMapping(value = "/centre", method = RequestMethod.GET)
	@ResponseBody
	public CommonResult getCentre(@RequestParam String robotID,
			@RequestParam String referralCode,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return userService.getCentre(robotID, referralCode);
	}
	
	/**
	 * 
	 * @param robotID 小助手ID
	 * @param tempID 用户临时ID
	 * @return
	 * TODO 更新此条记录为已点击
	 */
	@RequestMapping(value = "/url", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult updateH5Status(@RequestParam String robotID,
			@RequestParam String tempID,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return userService.updateH5Status(robotID, tempID);
	}
}
