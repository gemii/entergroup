package com.jl.nutcloud.core.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jl.nutcloud.core.domain.Admin;
import com.jl.nutcloud.core.otd.CommonResult;
import com.jl.nutcloud.core.service.ILoginService;


@Controller
@RequestMapping("/noauth/admin")
public class LoginController {

	@Autowired
	private ILoginService loginService;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult login(@RequestParam String username,
			@RequestParam String password,
			HttpSession session,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		CommonResult result = loginService.login(username,password);
		if(result.getStatus() == 200){
			session.setAttribute("admin", result.getData());
		}
		return result;
	}
	
	@RequestMapping(value = "/isLogin",  method = RequestMethod.GET)
	@ResponseBody
	public CommonResult isLogin(HttpSession session,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		Admin admin = (Admin)session.getAttribute("admin");
		if(admin == null){
			return CommonResult.build(-1, "not login");
		}else{
			return CommonResult.ok(admin);
		}
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	@ResponseBody
	public CommonResult logout(HttpSession session,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		session.removeAttribute("admin");
		return CommonResult.ok();
	}
}
