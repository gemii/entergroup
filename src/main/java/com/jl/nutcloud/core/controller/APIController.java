package com.jl.nutcloud.core.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jl.nutcloud.core.otd.CommonResult;
import com.jl.nutcloud.core.service.IAPIService;

/**
 * ClassName:APIController.java Function: this is a Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 17, 2017
 *
 * @see
 */
@Controller
@RequestMapping("/noauth/api")
public class APIController {

	@Autowired
	private IAPIService apiService;
	
	/**
	 * 
	 * @param openid 用户openid
	 * @return 
	 * TODO 接收金宝贝关注事件,发送入群指令消息
	 */
	@RequestMapping("/follow")
	@ResponseBody
	public CommonResult followed(@RequestParam String openid,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return apiService.sendGroupInstructionFollowed(openid);
	}
	
}
