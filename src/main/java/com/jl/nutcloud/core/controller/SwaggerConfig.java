package com.jl.nutcloud.core.controller;


/**
 * ClassName:SwaggerConfig.java Function: Reason: for swagger config
 * 
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 20, 2017
 *
 * @see
 */
//public class SwaggerConfig {
//	
//	private SpringSwaggerConfig springSwaggerConfig;
//
//	/**
//	 * Required to autowire SpringSwaggerConfig
//	 */
//	
//	@Autowired
//	public void setSpringSwaggerConfig(SpringSwaggerConfig springSwaggerConfig) {
//		this.springSwaggerConfig = springSwaggerConfig;
//	}
//
//	/**
//	 * Every SwaggerSpringMvcPlugin bean is picked up by the swagger-mvc
//	 * framework - allowing for multiple swagger groups i.e. same code base
//	 * multiple swagger resource listings.
//	 */
//	@Bean
//	public SwaggerSpringMvcPlugin customImplementation() {
//		return new SwaggerSpringMvcPlugin(this.springSwaggerConfig).apiInfo(
//				apiInfo()).includePatterns(".*?");
//	}
//
//	private ApiInfo apiInfo() {
//		ApiInfo apiInfo = new ApiInfo("My Apps API Title",
//				"My Apps API Description", "My Apps API terms of service",
//				"My Apps API Contact Email", "My Apps API Licence Type",
//				"My Apps API License URL");
//		return apiInfo;
//	}
//}
