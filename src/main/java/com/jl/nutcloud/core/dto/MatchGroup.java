package com.jl.nutcloud.core.dto;

import java.io.Serializable;

public class MatchGroup implements Serializable{

	private String roomId;
	private String robotId;
	
	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getRobotId() {
		return robotId;
	}

	public void setRobotId(String robotId) {
		this.robotId = robotId;
	}
}
