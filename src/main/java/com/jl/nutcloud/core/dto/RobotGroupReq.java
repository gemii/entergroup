package com.jl.nutcloud.core.dto;
/**
 * ClassName:RobotGroupReq.java Function: Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 20, 2017
 *
 * @see
 */
public class RobotGroupReq {

	private Integer userID;
	
	private String groupName;
	
	private String robotID;

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getRobotID() {
		return robotID;
	}

	public void setRobotID(String robotID) {
		this.robotID = robotID;
	}
	
}
