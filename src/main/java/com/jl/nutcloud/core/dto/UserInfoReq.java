package com.jl.nutcloud.core.dto;

public class UserInfoReq extends UserProfile{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5073382370233250753L;

	private String referralCode;

	private String centre;

	private String tempID;

	private String robotID;

	private String phoneCode;

	private Integer isFans;
	
	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getCentre() {
		return centre;
	}

	public void setCentre(String centre) {
		this.centre = centre;
	}

	public String getTempID() {
		return tempID;
	}

	public void setTempID(String tempID) {
		this.tempID = tempID;
	}

	public String getRobotID() {
		return robotID;
	}

	public void setRobotID(String robotID) {
		this.robotID = robotID;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	public Integer getIsFans() {
		return isFans;
	}

	public void setIsFans(Integer isFans) {
		this.isFans = isFans;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
