package com.jl.nutcloud.core.dto;

import java.io.Serializable;

public class BabyProfile implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 3579346667956364106L;

	private String babyName;
	
	private String babyBirth;
	
	private Integer babyGender;
	
	private String babyRelation;

	public String getBabyName() {
		return babyName;
	}

	public void setBabyName(String babyName) {
		this.babyName = babyName;
	}

	public String getBabyBirth() {
		return babyBirth;
	}

	public void setBabyBirth(String babyBirth) {
		this.babyBirth = babyBirth;
	}

	public Integer getBabyGender() {
		return babyGender;
	}

	public void setBabyGender(Integer babyGender) {
		this.babyGender = babyGender;
	}

	public String getBabyRelation() {
		return babyRelation;
	}

	public void setBabyRelation(String babyRelation) {
		this.babyRelation = babyRelation;
	}
	
}
