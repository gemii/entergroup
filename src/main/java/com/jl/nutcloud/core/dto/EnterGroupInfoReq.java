package com.jl.nutcloud.core.dto;

import java.io.Serializable;

/**
 * ClassName:EnterGroupInfoReq.java Function: Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 20, 2017
 *
 * @see
 */
public class EnterGroupInfoReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9212578756085836623L;
	
	private String robotID;
	
	private String username;
	
	private String code;
	
	private String status;

	public String getRobotID() {
		return robotID;
	}

	public void setRobotID(String robotID) {
		this.robotID = robotID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
