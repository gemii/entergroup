package com.jl.nutcloud.core.dto;

public class PageProfile {

	private Integer page;
	
	private Integer pageSize;

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
	/**
	 * 
	 * @param total 总记录数
	 * @param pageSize 每页大小
	 * @return
	 * TODO
	 */
	public static int computePages(int total ,int pageSize){
		int pages = total % pageSize;
		if(pages == 0 ){
			pages = total / pageSize;
		}else{
			pages = (total / pageSize) + 1;
		}
		return pages;
	}
}
