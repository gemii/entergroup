package com.jl.nutcloud.core.service;

import com.jl.nutcloud.core.otd.CommonResult;

/**
 * ClassName:IAssistantService.java Function: the interfaces to assistant Reason: for assistant
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 20, 2017
 *
 * @see
 */
public interface IAssistantService {

	CommonResult robotsList(Integer userID, String robotName);

	CommonResult addRobot(Integer userID);

	CommonResult restartRobot(Integer userID, String tag);

	CommonResult delRobot(String tag);

}
