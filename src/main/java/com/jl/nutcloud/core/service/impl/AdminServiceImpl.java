package com.jl.nutcloud.core.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jl.nutcloud.core.dto.EnterGroupInfoReq;
import com.jl.nutcloud.core.dto.PageProfile;
import com.jl.nutcloud.core.dto.RobotGroupReq;
import com.jl.nutcloud.core.mapper.AdminMapper;
import com.jl.nutcloud.core.otd.CommonResult;
import com.jl.nutcloud.core.service.IAdminService;

/**
 * ClassName:AdminServiceImpl.java Function: the interface to administrators IMPLEMENTATION Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 20, 2017
 *
 * @see
 */
@Service
public class AdminServiceImpl implements IAdminService{

	@Autowired
	private AdminMapper adminMapper;
	
	/**
	 * 
	 * @param userID 用户ID
	 * @param groupName 搜索的群名
	 * @param page 页码包装类
	 * @param response
	 * @return
	 * TODO 根据groupName page 获取群列表
	 */
	@Override
	public CommonResult groupList(Integer userID, String _groupName, PageProfile _page) {
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("groupName", "%" + _groupName + "%");
			param.put("clerkID", userID);
			param.put("start", (_page.getPage() - 1) * _page.getPageSize());
			param.put("end", _page.getPageSize());
			Integer groupsTotalCount = adminMapper.groupsTotalCount(param);
			Map<String, Object> resp = new HashMap<String, Object>();
			resp.put("groups", adminMapper.groupList(param));
			resp.put("totalCount", groupsTotalCount);
			resp.put("totalPage", PageProfile.computePages(groupsTotalCount, _page.getPageSize()));
			return CommonResult.ok(resp);
		} catch (Exception e) {
			e.printStackTrace();
			return CommonResult.build(-1, "server error");
		}
	}

	/**
	 * 
	 * @param enterInfoReq 请求包装类
	 * @param page 页码包装类
	 * @return
	 * TODO
	 */
	@Override
	public CommonResult enterInfo(EnterGroupInfoReq _enterInfoReq, PageProfile _page) {
		try {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("start", (_page.getPage() - 1) * _page.getPageSize());
			param.put("end", _page.getPageSize());
			_enterInfoReq.setCode("%" + _enterInfoReq.getCode() + "%");
			_enterInfoReq.setUsername("%" + _enterInfoReq.getUsername() + "%");
			param.put("info", _enterInfoReq);
			Integer totalCount = adminMapper.enterInfoTotalCount(param);
			Map<String, Object> resp = new HashMap<String, Object>();
			resp.put("infos", adminMapper.enterInfo(param));
			resp.put("totalCount", totalCount);
			resp.put("totalPage", PageProfile.computePages(totalCount, _page.getPageSize()));
			return CommonResult.ok(resp);
		} catch (Exception e) {
			e.printStackTrace();
			return CommonResult.build(-1, "server error");
		}
	}

	/**
	 * 
	 * @param robotGroupReq 请求包装类
	 * @param page 页码包装类
	 * @return
	 * TODO 小助手所在群信息
	 */
	@Override
	public CommonResult getRobotGroup(RobotGroupReq _robotGroupReq,
			PageProfile _page) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("start", (_page.getPage() - 1) * _page.getPageSize());
		param.put("end", _page.getPageSize());
		_robotGroupReq.setGroupName("%" + _robotGroupReq.getGroupName() + "%");
		param.put("info", _robotGroupReq);
		Integer robotGroupCount = adminMapper.getRobotGroupCount(param);
		Map<String, Object> resp = new HashMap<String, Object>();
		resp.put("infos", adminMapper.getRobotGroup(param));
		resp.put("totalCount", robotGroupCount);
		resp.put("totalPage", PageProfile.computePages(robotGroupCount, _page.getPageSize()));
		return CommonResult.ok(resp);
	}

	/**
	 * 
	 * @param id 入群记录ID
	 * @param status 入群状态
	 * @return
	 * TODO 管理员修改  ID记录的入群状态
	 */
	@Override
	public CommonResult updateEnterGroupStatus(Integer id, String status) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", id);
		param.put("status", status);
		adminMapper.updateEnterGroupStatus(param);
		return CommonResult.ok();
	}

}
