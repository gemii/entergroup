package com.jl.nutcloud.core.service;

import com.jl.nutcloud.core.otd.CommonResult;

/**
 * ClassName:ILoginService.java Function: Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 30, 2017
 *
 * @see
 */
public interface ILoginService {

	CommonResult login(String username, String password);

}
