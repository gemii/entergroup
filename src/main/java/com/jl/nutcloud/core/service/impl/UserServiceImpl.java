package com.jl.nutcloud.core.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jl.nutcloud.core.component.matchGroup.IMatchGroupStrategy;
import com.jl.nutcloud.core.component.matchGroup.MatchGroupContext;
import com.jl.nutcloud.core.component.matchGroup.MatchGroupContext.MatchGroupStrategy;
import com.jl.nutcloud.core.component.matchGroup.MatchGroupResult;
import com.jl.nutcloud.core.constant.RedisSettingConstant;
import com.jl.nutcloud.core.constant.UserSettingConstant;
import com.jl.nutcloud.core.dto.BabyProfile;
import com.jl.nutcloud.core.dto.MatchGroup;
import com.jl.nutcloud.core.dto.UserInfoReq;
import com.jl.nutcloud.core.mapper.UniqueCodeMapper;
import com.jl.nutcloud.core.mapper.UserMapper;
import com.jl.nutcloud.core.otd.CommonResult;
import com.jl.nutcloud.core.service.IAPIService;
import com.jl.nutcloud.core.service.IUserService;
import com.jl.nutcloud.core.util.HttpClientUtil;
import com.jl.nutcloud.core.util.SMSUtil;
import com.jl.nutcloud.core.util.UUIDUtils;
import com.jl.nutcloud.core.util.XMLUtil;

/**
 * ClassName:UserServiceImpl.java Function: it's a user enter group implementation Reason: for enter group
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 16, 2017
 *
 * @see
 */
@Service
public class UserServiceImpl implements IUserService{
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private UniqueCodeMapper uniqueCodeMapper;
	
	@Autowired
	private IAPIService apiService;
	@Autowired
	private MatchGroupContext matchGroupContext;
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	public CommonResult submitForm(UserInfoReq _userInfo, BabyProfile _babyInfo) {
//		验证手机号
		String redis_code = RedisSettingConstant.hget(RedisSettingConstant.PHONE_CODE_KEY, _userInfo.getPhone());
		if(redis_code == null || !redis_code.equals(_userInfo.getPhoneCode())){
			return CommonResult.build(-2, "phone code error");
		}
		RedisSettingConstant.hdel(RedisSettingConstant.PHONE_CODE_KEY, _userInfo.getPhone());
		
//		处理表单信息
		MatchGroup matchGroup = this.handleFormInfo(_userInfo, _babyInfo);
//		需求修改放弃该步骤
//		验证是否粉丝和会员
//		Map<String, Object> vip_fan_map = this.validateFanAndVIP(_userInfo.getOpenid(), _userInfo.getPhone());
//		发送入群指令
//		apiService.sendGroupInstructionFollowed(_userInfo.getOpenid());
		if(matchGroup == null){
			return CommonResult.build(-1, "match fail");
		}else{
			return CommonResult.ok(matchGroup);
		}
	}

	private Map<String, Object> validateFanAndVIP(String _openid, String _phone) {
		try {
			Map<String, String> param = new HashMap<String, String>();
			param.put("openid", _openid);
			param.put("phone", _phone);
			System.out.println(param);
			String result = HttpClientUtil.doPost(UserSettingConstant.VALIDATE_VPI_FAN_URL, param);
			System.out.println(result);
			Map<String, Object> resultMap = XMLUtil.xml2map(result);
			Map<String, Object> statusMap = (Map<String, Object>) resultMap.get("status");
			if(UserSettingConstant.VALIDATE_VPI_FAN_SUCCESS.equals(String.valueOf(statusMap.get("status")))){
				return (Map<String, Object>) resultMap.get("data");
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private MatchGroup handleFormInfo(UserInfoReq _userInfo, BabyProfile _babyInfo) {
		
//		匹配群
		Map<String,String> conds = new HashMap<String,String>();
		conds.put("centreName",_userInfo.getCentre());
		MatchGroupResult matchResult = matchGroupContext.matchGroup(MatchGroupStrategy.GYMBOREE_MATCHGROUP_STRATEGY,conds);
		
		MatchGroup matchGroup = null;
		if(matchResult.getResultCode() == 1){
			matchGroup = userMapper.getMatchGroup(matchResult.getMatchGroupId());
		}
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("matchGroup", matchGroup == null ?"":matchGroup.getRoomId());
		param.put("robotID", matchGroup == null ?"":matchGroup.getRobotId());
		param.put("matchStatus", matchGroup == null ? UserSettingConstant.MATCH_ROOM_FAILED : UserSettingConstant.MATCH_ROOM_SUCCESS);
		param.put("babyInfo", _babyInfo);
		param.put("userInfo", _userInfo);
//		生成uuid
		param.put("nut_user_uuid", UUIDUtils.getUUID());
		param.put("baby_info_uuid", UUIDUtils.getUUID());
//		生成此用户referral code 为此用户手机号
		param.put("newReferralCode", _userInfo.getPhone());
//		插入用户平台信息
		userMapper.insertPlatFormUser(param);
//		插入 UserInfo TO BE Deprecated   
		userMapper.insertUserInfo(param);
		
		return matchGroup;
	}

	/**
	 * TODO 生成唯一referralcode
	 */
	private String generateReferralCode() {
		String referralCode = null;
		String referralCodeUUID = null;
		do {
			referralCode = UUIDUtils.getRandom(UserSettingConstant.REFERRAL_CODE_LENGTH);
			referralCodeUUID = uniqueCodeMapper.selectReferralCodeUUID(referralCode);
		} while (referralCodeUUID != null);
		return referralCode;
	}

	/**
	 * TODO 发送手机验证码短信
	 */
	@Override
	public CommonResult getPhoneCode(String _phoneNumber) {
		String code = SMSUtil.getCode();
		logger.info("_phoneNumber="+_phoneNumber+"--code="+code);
		RedisSettingConstant.hset(RedisSettingConstant.PHONE_CODE_KEY, _phoneNumber, code);
		SMSUtil.sendMsg_huanxin_post(_phoneNumber, code, UserSettingConstant.SMS_EXPIRE);
		return CommonResult.ok();
	}

	/**
	 * TODO 根据referralcode 获取中心名
	 */
	@Override
	public CommonResult getCentre(String _robotID, String _referralCode) {
		Map<String, String> param = new HashMap<String, String>();
		param.put("robotID", _robotID);
		param.put("referralCode", _referralCode);
		String centre = userMapper.getCentre(param);
		if(centre == null)
			return CommonResult.build(-1, "not exists");
		return CommonResult.ok(centre);
	}

	/**
	 * 
	 * @param robotID 小助手ID
	 * @param tempID 用户临时ID
	 * @return
	 * TODO 更新此条记录为已点击
	 */
	@Override
	public CommonResult updateH5Status(String robotID, String tempID) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("robotID", robotID);
		param.put("tempID", tempID);
		userMapper.updateH5Status(param);
		return CommonResult.ok();
	}

}
