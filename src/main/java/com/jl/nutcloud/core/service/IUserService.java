package com.jl.nutcloud.core.service;

import com.jl.nutcloud.core.dto.BabyProfile;
import com.jl.nutcloud.core.dto.UserInfoReq;
import com.jl.nutcloud.core.otd.CommonResult;

public interface IUserService {

	/**
	 * 
	 * @param formInfo 用户提交表单信息包装类
	 * @return
	 * TODO 用户提交表单
	 */
	CommonResult submitForm(UserInfoReq _userInfo, BabyProfile _babyInfo);

	/**
	 * 
	 * @param phoneNumber 手机号
	 * @return
	 * TODO 验证手机号时获取验证码
	 */
	CommonResult getPhoneCode(String _phoneNumber);

	/**
	 * 
	 * @param _referralCode 用户所填推荐码
	 * @param _robotID  
	 * @return
	 * TODO 根据用户所填推荐码返回对应的中心名
	 */
	CommonResult getCentre(String _robotID, String _referralCode);

	CommonResult updateH5Status(String robotID, String tempID);

}
