package com.jl.nutcloud.core.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jl.nutcloud.core.domain.Admin;
import com.jl.nutcloud.core.mapper.AdminMapper;
import com.jl.nutcloud.core.otd.CommonResult;
import com.jl.nutcloud.core.service.ILoginService;
import com.jl.nutcloud.core.util.MD5Utils;

/**
 * ClassName:LoginServiceImpl.java Function: Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 30, 2017
 *
 * @see
 */
@Service
public class LoginServiceImpl implements ILoginService{

	@Autowired
	private AdminMapper adminMapper;
	
	@Override
	public CommonResult login(String username, String password) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("username", username);
		param.put("password", MD5Utils.md5Encode(password));
		Admin admin = adminMapper.login(param);
		if(admin == null)
			return CommonResult.build(-1, "user not found");
		else
			return CommonResult.ok(admin);
	}

}
