package com.jl.nutcloud.core.service;

import com.jl.nutcloud.core.dto.EnterGroupInfoReq;
import com.jl.nutcloud.core.dto.PageProfile;
import com.jl.nutcloud.core.dto.RobotGroupReq;
import com.jl.nutcloud.core.otd.CommonResult;

/**
 * ClassName:IAdminService.java Function: the interface to administrators Reason: for administrators
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 20, 2017
 *
 * @see
 */
public interface IAdminService {

	CommonResult groupList(Integer userID, String _groupName, PageProfile _page);

	CommonResult enterInfo(EnterGroupInfoReq _enterInfoReq, PageProfile _page);

	CommonResult getRobotGroup(RobotGroupReq _robotGroupReq, PageProfile _page);

	CommonResult updateEnterGroupStatus(Integer id, String status);

}
