package com.jl.nutcloud.core.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.jl.nutcloud.core.constant.RedisSettingConstant;
import com.jl.nutcloud.core.constant.RobotSettingContant;
import com.jl.nutcloud.core.domain.JoinGroupInfo;
import com.jl.nutcloud.core.mapper.APIMapper;
import com.jl.nutcloud.core.otd.CommonResult;
import com.jl.nutcloud.core.service.IAPIService;

/**
 * ClassName:APIServiceImpl.java Function: it is a interface to other enterprises IMPLEMENTATION Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 17, 2017
 *
 * @see
 */
@Service
public class APIServiceImpl implements IAPIService{

	@Autowired
	private APIMapper apiMapper;
	
	/**
	 * 
	 * @param openid 用户openid
	 * TODO 接收金宝贝关注事件,发送入群指令消息
	 */
	@Override
	public CommonResult sendGroupInstructionFollowed(String _openid) {
		return this.sendGroupInstruction(apiMapper.selectJoinGroupInfo(_openid));
	}

	@Override
	public CommonResult sendGroupInstruction(JoinGroupInfo _info) {
		if(_info == null){
			return CommonResult.build(-1, "not found");
		}
		Map<String, Object> sendMap = new HashMap<String, Object>();
		sendMap.put("MsgType", 10001);
		Map<String, String> msg = new HashMap<String, String>();
		msg.put("Temp_ID", _info.getTempID());
		msg.put("Group_ID", _info.getGroupID());
		msg.put("Uin", _info.getRobotID());
		msg.put("Referral_Code", _info.getReferralCode());
		sendMap.put("Msg", msg);
		RedisSettingConstant.publishMessage(RobotSettingContant.tag2Channel(_info.getRobotTag()), 
				JSON.toJSONString(sendMap));
		return CommonResult.ok();
	}

}
