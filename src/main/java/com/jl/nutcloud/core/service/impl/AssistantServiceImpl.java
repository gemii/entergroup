package com.jl.nutcloud.core.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.jl.nutcloud.core.constant.RedisSettingConstant;
import com.jl.nutcloud.core.constant.RobotSettingContant;
import com.jl.nutcloud.core.mapper.AssistantMapper;
import com.jl.nutcloud.core.otd.CommonResult;
import com.jl.nutcloud.core.service.IAssistantService;
import com.jl.nutcloud.core.util.HttpClientUtil;

/**
 * ClassName:AssistantService.java Function: the interfaces to assistant IMPLEMENTATION Reason:for assistant
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 20, 2017
 *
 * @see
 */
@Service
public class AssistantServiceImpl implements IAssistantService{

	@Autowired
	private AssistantMapper assistantMapper;
	
	/**
	 * 
	 * @param userID  用户ID
	 * @return
	 * TODO 获取此用户的机器人列表
	 */
	@Override
	public CommonResult robotsList(Integer _userID, String _robotName) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("userID", _userID);
		param.put("role", "2");
		param.put("robotName", "%" + _robotName + "%");
		return CommonResult.ok(assistantMapper.robotsList(param));
	}

	/**
	 * 
	 * @param userID 用户ID
	 * @return
	 * TODO 用户添加小助手
	 */
	@Override
	public CommonResult addRobot(Integer userID) {
		//生成新的机器人标签
		String tag = RedisSettingConstant.get(RedisSettingConstant.ROBOT_MAX_TAG);
		if(tag == null){
			String max_tag = assistantMapper.getRobotMaxTag();
			RedisSettingConstant.set(RedisSettingConstant.ROBOT_MAX_TAG, max_tag);
		}
		tag = String.valueOf(RedisSettingConstant.incr(RedisSettingConstant.ROBOT_MAX_TAG));
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("userID", userID);
		param.put("tag", tag);
		param.put("role","2");
		assistantMapper.insertClerkRobot(param);
		String url = this.startRobot(userID, tag, "true");
		if(url == null)
			return CommonResult.build(-1, "url not found");
		else{
			return CommonResult.ok(url);
		}
	}

	/**
	 * 
	 * @param userID 用户ID
	 * @param tag 机器人标签
	 * @return
	 * TODO 用户重启小助手
	 */
	@Override
	public CommonResult restartRobot(Integer userID, String tag) {
		String url = this.startRobot(userID, tag, "");
		if(url == null)
			return CommonResult.build(-1, "url not found");
		else if(RobotSettingContant.UNKOWN_ROBOT_TAG.equals(url)){
			return this.addRobot(userID);
		}else{
			return CommonResult.ok(url);
		}
	}

	private String startRobot(Integer userID, String tag, String isNew){
		try {
			//发送请
			String url = RobotSettingContant.START_ROBOT_URL.replace("BOT", tag).replace("NEW", isNew);
			String responseJson = HttpClientUtil.doGet(url);
			Map<String, String> responseMap = (Map<String, String>) JSON.parse(responseJson);
			if(RobotSettingContant.UNKOWN_ROBOT_TAG.equals(String.valueOf(responseMap.get("code")))){
				return RobotSettingContant.UNKOWN_ROBOT_TAG;
			}
			//请求发送成功之后查询redis
			if(RobotSettingContant.START_ROBOT_SUCCESS.equals(String.valueOf(responseMap.get("code")))){
				for (int i = 0; i < 3; i++) {
					String urlJson = RedisSettingConstant.hget(RedisSettingConstant.START_ROBOT_KEY, tag);
					if(urlJson != null){
						//删除redis记录
						Map<String, String> urlMap = (Map<String, String>) JSON.parse(urlJson);
						RedisSettingConstant.hdel(RedisSettingConstant.START_ROBOT_KEY, tag);
						return urlMap.get("url");
					}
					Thread.sleep(2500);
				}
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}

	/**
	 * 
	 * @param tag 机器人标签
	 * @return
	 * TODO 用户删除小助手
	 */
	@Override
	public CommonResult delRobot(String tag) {
		assistantMapper.delRobot(tag);
		return CommonResult.ok();
	}
}
