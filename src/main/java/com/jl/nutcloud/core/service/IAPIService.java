package com.jl.nutcloud.core.service;

import com.jl.nutcloud.core.domain.JoinGroupInfo;
import com.jl.nutcloud.core.otd.CommonResult;

/**
 * ClassName:IAPIService.java Function: interface to other enterprises Reason:for other enterprises
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 17, 2017
 *
 * @see
 */
public interface IAPIService {

	CommonResult sendGroupInstructionFollowed(String _openid);
	
	CommonResult sendGroupInstruction(JoinGroupInfo _info);

}
