package com.jl.nutcloud.core.component.matchGroup;

import java.util.Map;

public interface IMatchGroupStrategy {

	public MatchGroupResult matchGroup(Map<String,String> _matchingconds);
	
}
