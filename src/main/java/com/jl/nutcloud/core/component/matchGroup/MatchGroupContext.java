package com.jl.nutcloud.core.component.matchGroup;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;


/**
 * 根据给定条件匹配合适的群,匹配策略选择容器
 * */
@Component
public class MatchGroupContext {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private IMatchGroupStrategy gymboreeMatchGroupStrategy;
	
	/**
	 * 容器匹配策略枚举
	 * */
	public enum MatchGroupStrategy{
		/**
		 * 金宝贝入群策略
		 * */
		GYMBOREE_MATCHGROUP_STRATEGY
	}
	
	/**
	 * 匹配群
	 * */
	public MatchGroupResult matchGroup(MatchGroupStrategy _strategy,Map<String,String> _params){
		logger.info("matchGroup-start--strategy="+_strategy+"--params="+JSONObject.toJSONString(_params));
		MatchGroupResult matchGroupResult = null;
		switch (_strategy) {
			case GYMBOREE_MATCHGROUP_STRATEGY:
				matchGroupResult = gymboreeMatchGroupStrategy.matchGroup(_params);
				break;
			default:
				break;
		}
		logger.info("matchGroup-end--strategy="+_strategy+"--matchGroupResult="+JSONObject.toJSONString(matchGroupResult));
		return matchGroupResult;
	}
}
