package com.jl.nutcloud.core.component.matchGroup.strategy;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jl.nutcloud.core.component.matchGroup.IMatchGroupStrategy;
import com.jl.nutcloud.core.component.matchGroup.MatchGroupResult;
import com.jl.nutcloud.core.mapper.UserMapper;

/**
 * 好奇入群匹配策略
 * */
@Component("gymboreeMatchGroupStrategy")
public class GymboreeMatchGroupStrategy implements IMatchGroupStrategy {
	
	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 匹配群结果映射
	 * */
	private static Map<Integer,String> resultCodeMap = new HashMap<Integer,String>();
	static{
		resultCodeMap.put(1, "匹配成功");
		resultCodeMap.put(-1, "群匹配失败");
	}
	
	@Autowired
	private UserMapper userMapper;
	
	@Override
	public MatchGroupResult matchGroup(Map<String, String> _matchingconds) {
		String centreName = _matchingconds.get("centreName");
		
		String roomId = userMapper.matchGroup(centreName);
		if(StringUtils.isBlank(roomId)){
			logger.info("matchGroup--_matchingconds="+JSONObject.toJSONString(_matchingconds)+"--没有匹配到合适的群");
			return buildMatchGroupResult(-1,null);
		}
		logger.info("matchGroup--_matchingconds="+JSONObject.toJSONString(_matchingconds)+"--roomId="+roomId);
		return buildMatchGroupResult(1,roomId);
	}
	
	private MatchGroupResult buildMatchGroupResult(int _resultCode,String _matchGroupId){
		MatchGroupResult result = new MatchGroupResult();
		result.setResultCode(_resultCode);
		result.setMsg(resultCodeMap.get( _resultCode));
		result.setMatchGroupId(_matchGroupId);
		return result;
	}
	
}
