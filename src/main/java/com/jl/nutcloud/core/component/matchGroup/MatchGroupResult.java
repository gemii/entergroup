package com.jl.nutcloud.core.component.matchGroup;

public class MatchGroupResult {

	private int resultCode;
	private String msg;
	private String matchGroupId;
	
	public int getResultCode() {
		return resultCode;
	}
	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getMatchGroupId() {
		return matchGroupId;
	}
	public void setMatchGroupId(String matchGroupId) {
		this.matchGroupId = matchGroupId;
	}
	
}
