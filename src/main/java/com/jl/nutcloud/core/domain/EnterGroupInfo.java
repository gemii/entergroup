package com.jl.nutcloud.core.domain;
/**
 * ClassName:EnterGroupInfo.java Function: Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 20, 2017
 *
 * @see
 */
public class EnterGroupInfo {

	private Integer id;
	
	private String userName;
	
	private String groupName;
	
	private String enterGroupStatus;
	
	private String H5Status;
	
	private String referralCode;
	
	private String remindMessage;
	
	private String centre;

	private String submitTime;
	
	private String phoneNumber;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getEnterGroupStatus() {
		return enterGroupStatus;
	}

	public void setEnterGroupStatus(String enterGroupStatus) {
		this.enterGroupStatus = enterGroupStatus;
	}

	public String getH5Status() {
		return H5Status;
	}

	public void setH5Status(String h5Status) {
		H5Status = h5Status;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getRemindMessage() {
		return remindMessage;
	}

	public void setRemindMessage(String remindMessage) {
		this.remindMessage = remindMessage;
	}

	public String getCentre() {
		return centre;
	}

	public void setCentre(String centre) {
		this.centre = centre;
	}

	public String getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(String submitTime) {
		this.submitTime = submitTime;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
}
