package com.jl.nutcloud.core.domain;
/**
 * ClassName:JoinGroupInfo.java Function: domain entity Reason: join group info
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 17, 2017
 *
 * @see
 */
public class JoinGroupInfo {

	private String groupID;
	
	private String tempID;

	private String robotTag;
	
	private String robotID;

	private String referralCode;
	
	public String getGroupID() {
		return groupID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	public String getTempID() {
		return tempID;
	}

	public void setTempID(String tempID) {
		this.tempID = tempID;
	}

	public String getRobotTag() {
		return robotTag;
	}

	public void setRobotTag(String robotTag) {
		this.robotTag = robotTag;
	}

	public String getRobotID() {
		return robotID;
	}

	public void setRobotID(String robotID) {
		this.robotID = robotID;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	
}
