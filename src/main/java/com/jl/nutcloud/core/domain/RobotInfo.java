package com.jl.nutcloud.core.domain;
/**
 * ClassName:RobotInfo.java Function: Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 20, 2017
 *
 * @see
 */
public class RobotInfo {

	private String robotID;
	
	private String robotName;

	private String robotTag;
	
	private String status;
	
	private String unifiedURL;
	
	public String getRobotID() {
		return robotID;
	}

	public void setRobotID(String robotID) {
		this.robotID = robotID;
	}

	public String getRobotName() {
		return robotName;
	}

	public void setRobotName(String robotName) {
		this.robotName = robotName;
	}

	public String getRobotTag() {
		return robotTag;
	}

	public void setRobotTag(String robotTag) {
		this.robotTag = robotTag;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUnifiedURL() {
		return unifiedURL;
	}

	public void setUnifiedURL(String unifiedURL) {
		this.unifiedURL = unifiedURL;
	}

}
