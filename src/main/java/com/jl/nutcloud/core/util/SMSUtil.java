package com.jl.nutcloud.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSON;

/**
 * ClassName:SMSUtils.java Function:it's a send SMS IMPLEMENTATION Reason:for send SMS
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 17, 2017
 *
 * @see
 */
public class SMSUtil {

	private static String Tencent_URL_V5 = "https://yun.tim.qq.com/v5/tlssmssvr/sendsms?sdkappid=APPID&random=RANDOM";
	private static String AppId = "1400014204";
	private static String AppKey = "249bbc86b90f9eec2805fe048956557d";
	private static String SIG = "appkey=APPKEY&random=RANDOM&time=TIMESTAMP&mobile=MOBILE";
	
	private static String ZhongLan_URL = "http://www.wemediacn.net/api/smsservice.asmx/SendSMS?mobile=MOBILE&Content=CONTENT&TokenID=TOKENID";
	private static String TokenID = "611408984465753557160497";
	private static String Content = "[栗子妈妈]您的验证码为code,有效期time.";

//	private static String Netease_URL = "https://api.netease.im/sms/sendtemplate.action";
//	private static String Netease_AppKey = "d42348bf2068721a66bb89347a184cf9";
//	private static String Netease_AppSecret = "3e20a7259b5f";
	
	private static String Huanxin_URL = "http://123.56.206.101:8082/MWGate/wmgw.asmx/MongateCsSpSendSmsNew";
	private static String userId = "SHJLSC";
	private static String password = "SHJLSC1";
	
	public static final String SEND_SUCCESS = "success";
	
	/**
	 * 
	 * @param phone
	 * @param validateCode
	 * @param time
	 * @return
	 * TODO 腾讯SMS
	 */
	public static String sendMsg_Tencent_v5(String phone, String validateCode, String time) {
		String random = String.valueOf((int) (Math.random() * 10000));
		String timestamp = String.valueOf((System.currentTimeMillis()/1000));
		String url = Tencent_URL_V5.replace("APPID", AppId).replace("RANDOM",random);
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, String> telMap = new HashMap<String, String>();
		telMap.put("nationcode", "86");
		telMap.put("mobile", phone);
		map.put("tel", telMap);
		map.put("type", "0");
		map.put("msg", "您的验证码为" + validateCode + "，请于" + time + "分钟内填写。如非本人操作，请忽略本短信。");
		map.put("sig", SHA1Util.Encrypt(SIG.replace("APPKEY", AppKey).replace("RANDOM", random).replace("TIMESTAMP", timestamp).replace("MOBILE", phone), "SHA-256"));
		map.put("time", timestamp);
		map.put("extend", "");
		map.put("ext", "");
		Map<String, Object> resultMap = (Map<String, Object>) JSON.parse(HttpClientUtil.doPostJson(url, JSON.toJSONString(map)));
		String result = String.valueOf(resultMap.get("result"));
		if("0".equals(result)){
			return SEND_SUCCESS;
		}else{
			return result;
		}
	}
	
	/**
	 * 
	 * @param Phone
	 * @param start
	 * @param code
	 * @param time
	 * @return
	 * TODO 众览SMS
	 */
	public static String sendMsg_zhonglan(String Phone,
			Object code, Object time) {
		try {
			String content = Content.replace("code", String.valueOf(code))
					.replace("time", String.valueOf(time));
			Map<String, Object> map = XMLUtil.xml2map(HttpClientUtil.doGet(ZhongLan_URL.replace("MOBILE", Phone)
							.replace("CONTENT", content)
							.replace("TOKENID", TokenID)));
			String result = String.valueOf(map.get("string"));
			if(result.startsWith("OK")){
				return SEND_SUCCESS;
			}else{
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 
	 * TODO 网易云信SMS
	 */
//	public static String sendMsg_Netease(List<String> mobiles, String templateid, List<String> params) {
//		Map<String, String> headers = new HashMap<String, String>();
//		headers.put("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
//		headers.put("AppKey", Netease_AppKey);
//		String curTime = System.currentTimeMillis() + "";
//		headers.put("CurTime", curTime);
//		String nonce = UUIDUtils.getId(20);
//		headers.put("Nonce", nonce);
//		headers.put("CheckSum", SHA1Util.Encrypt(Netease_AppSecret + nonce + curTime, "SHA-1"));
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("mobiles", JSON.toJSONString(mobiles));
//		map.put("templateid", templateid);  //3029701
//		map.put("params", JSON.toJSONString(params));
//		return HttpClientUtil.doPost(Netease_URL, map, headers);
//	}

	/**
	 * 
	 * @param mobiles 手机列表
	 * @return
	 * TODO 环信SMS post
	 */
	public static String sendMsg_huanxin_post(List<String> mobiles, String code, String time){
		try {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Content-Type", "application/x-www-form-urlencoded");
			Map<String, String> param = new LinkedHashMap<String, String>();
			param.put("userId", userId);
			param.put("password", password);
			param.put("pszMobis", StringUtils.join(mobiles, ","));
			param.put("pszMsg", "您的验证码为" + code + "，有效期为" + time + "。如非本人操作，请忽略本短信。");
			param.put("iMobiCount", mobiles.size() + "");
			param.put("pszSubPort", "*");
			Map<String, Object> map = XMLUtil.xml2map(HttpClientUtil.doPost(Huanxin_URL, param));
			String result = String.valueOf(map.get("string"));
			System.out.println(result);
			if(result.length() > 10){
				return SEND_SUCCESS;
			}else{
				return result;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return "server error";
		}
	}
	
	public static String sendMsg_huanxin_post(String mobile, String code, String time){
		List<String> mobiles = new ArrayList<String>();
		mobiles.add(mobile);
		return sendMsg_huanxin_post(mobiles, code, time);
	}
	
	/**
	 * 
	 * @param mobiles 手机列表
	 * @return
	 * TODO 环信SMS get
	 */
	public static String sendMsg_Huanxin_get(List<String> mobiles){
		return HttpClientUtil.doGet("http://123.56.206.101:8082/MWGate/wmgw.asmx/MongateCsSpSendSmsNew?"
				+ "userId=CS0001&password=123456&pszMobis=PHONE&pszMsg=同事您好，感谢您对此次测试的配合。&iMobiCount=SIZE&pszSubPort=*"
				.replace("PHONE", StringUtils.join(mobiles, ",").replace("SIZE", mobiles.size() + "")));
	}
	

	public static String getCode() {
		return String.valueOf((int) (Math.random() * 9000 + 1000));
	}
	
	public static String sendMsg(String phone, String code){
		return sendMsg_huanxin_post(phone, code, "60分钟");
	}
	
	public static String resendMsg(String phone, String code){
		return sendMsg_zhonglan(phone, code, "60分钟");
	}
	
}

