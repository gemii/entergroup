package com.jl.nutcloud.core.mapper;

import java.util.Map;

import com.jl.nutcloud.core.dto.MatchGroup;
import com.jl.nutcloud.core.dto.UserInfoReq;

public interface UserMapper {

	public String matchGroup(String centreName);
	public MatchGroup getMatchGroup(String roomId);

	public Integer selectUserStatusID(Map<String, Object> param);

	public void updateUserStatus(Map<String, Object> param);

	public void insertUserInfo(Map<String, Object> param);

	public void insertPlatFormUser(Map<String, Object> param);

	public String getCentre(Map<String, String> param);

	public void updateH5Status(Map<String, Object> param);

	public int insertuserStatus(Map<String, Object> param);
}
