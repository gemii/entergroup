package com.jl.nutcloud.core.mapper;

import java.util.List;
import java.util.Map;

import com.jl.nutcloud.core.domain.RobotInfo;


/**
 * ClassName:AssistantMapper.java Function: it is a interface to api mapper Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 17, 2017
 *
 * @see
 */
public interface AssistantMapper {

	List<RobotInfo> robotsList(Map<String, Object> param);

	void insertClerkRobot(Map<String, Object> param);

	String getRobotMaxTag();

	void delRobot(String tag);

}
