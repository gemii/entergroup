package com.jl.nutcloud.core.mapper;

import java.util.List;
import java.util.Map;

import com.jl.nutcloud.core.domain.Admin;
import com.jl.nutcloud.core.domain.EnterGroupInfo;
import com.jl.nutcloud.core.domain.GroupInfo;
import com.jl.nutcloud.core.domain.RobotGroupInfo;

/**
 * ClassName:AdminMapper.java Function: it is a interface to api mapper Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 17, 2017
 *
 * @see
 */
public interface AdminMapper {

	List<GroupInfo> groupList(Map<String, Object> param);

	Integer groupsTotalCount(Map<String, Object> param);

	List<EnterGroupInfo> enterInfo(Map<String, Object> param);

	Integer enterInfoTotalCount(Map<String, Object> param);

	List<RobotGroupInfo> getRobotGroup(Map<String, Object> param);

	Integer getRobotGroupCount(Map<String, Object> param);

	void updateEnterGroupStatus(Map<String, Object> param);

	Admin login(Map<String, Object> param);

}
