package com.jl.nutcloud.core.mapper;

public interface UniqueCodeMapper {

	String selectReferralCodeUUID(String referralCode);

}
