package com.jl.nutcloud.core.mapper;

import com.jl.nutcloud.core.domain.JoinGroupInfo;

/**
 * ClassName:APIMapper.java Function: it is a interface to api mapper Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 17, 2017
 *
 * @see
 */
public interface APIMapper {

	JoinGroupInfo selectJoinGroupInfo(String _openid);

	String getRobotTagByOpenID(String _openid);

}
