package com.jl.nutcloud.core.constant;

public class UserSettingConstant {

//	群匹配状态
	public static final String MATCH_ROOM_SUCCESS = "群匹配成功";
	public static final String MATCH_ROOM_FAILED = "群匹配失败";
	
//	验证粉丝及会员url
	public static final String VALIDATE_VPI_FAN_URL = "http://wx.gymbomate.com/GymboreeHOServices/WXService.asmx/IsVipOrFan";
	
	public static final String VALIDATE_VPI_FAN_SUCCESS = "0";
	
//	短信验证码有效时间
	public static final String SMS_EXPIRE = "5分钟";
	
//	是粉丝
	public static final Integer IS_FAN = 1;
	
//	referral code length
	public static final Integer REFERRAL_CODE_LENGTH = 4;
	
//	from wechat' gender to project' gender
	public static int wgender2pgender(int wgender){
		if(wgender == 0){
			return 2;
		}else if(wgender == 1){
			return 0;
		}else{
			return 1;
		}
	}
	
}
