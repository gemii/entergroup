package com.jl.nutcloud.core.constant;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * ClassName:RedisSettingConstant.java Function: redis configuration Reason:for redis module
 * 
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 20, 2017
 *
 * @see
 */
public class RedisSettingConstant {

//	手机号验证码存储key值
	public static final String PHONE_CODE_KEY = "phone_code";
	
//	机器人登陆二维码key值
	public static final String START_ROBOT_KEY = "login";

//	机器人tag key 值
	public static final String ROBOT_MAX_TAG = "max_tag"; 
	
//	************************  redis setting ************************
	
	// Redis服务器IP
	private static String ADDRESS;

	// Redis的端口号
	private static int PORT;

	// 可用连接实例的最大数目，默认值为8；
	// 如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
	private static int MAX_ACTIVE;

	// 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值也是8。
	private static int MAX_IDLE;

	// 等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException；
	private static int MAX_WAIT;

	// 超时时间
	private static int TIMEOUT;

	private static JedisPool jedisPool = null;

	private static String realIP = null;

	/**
	 * redis过期时间,以秒为单位
	 */
	public final static int EXRP_HOUR = 60 * 60; // 一小时
	public final static int EXRP_DAY = 60 * 60 * 24; // 一天
	public final static int EXRP_MONTH = 60 * 60 * 24 * 30; // 一个月

	public RedisSettingConstant(String address,int port,int maxActive,int maxIdle,int maxWait,int timeout) {
		this.ADDRESS = address;
		this.PORT = port;
		this.MAX_ACTIVE = maxActive;
		this.MAX_IDLE = maxIdle;
		this.MAX_WAIT = maxWait;
		this.TIMEOUT = timeout;
		getJedisPool();
	}
	
	public synchronized static JedisPool getJedisPool() {
		if (jedisPool == null) {
			JedisPoolConfig config = new JedisPoolConfig();
			config.setMaxTotal(MAX_ACTIVE);
			config.setMaxIdle(MAX_IDLE);
			config.setMaxWaitMillis(MAX_WAIT);
			jedisPool = new JedisPool(config, ADDRESS, PORT, TIMEOUT);
			return jedisPool;
		}
		return jedisPool;
	}

	public static void destroyJedisPool() {
		jedisPool.destroy();
	}

	public synchronized static Jedis getResource() {
		try {
			Jedis jedis = null;
			jedis = getJedisPool().getResource();
//			jedis.auth(AUTH);
			return jedis;
		} catch (Exception e) {
			System.out.println("jedis pool is already closed");
			e.printStackTrace();
			return null;
		}
	}

	public static void returnResource(Jedis jedis) {
		try {
			if (jedis != null) {
				jedisPool.returnResource(jedis);
			}
		} catch (Exception e) {
		}
	}

	public static void publishMessage(String channel, String message) {
		Jedis jedis = getResource();
		jedis.publish(channel, message);
		returnResource(jedis);
	}

	public static String getIP() {
		return ADDRESS;
	}

	public static String set(String key, String value) {
		return set(null, key, value);
	}

	public static String set(Integer index, String key, String value) {
		Jedis jedis = getResource();
		jedis.select(index == null ? 0 : index);
		String result = jedis.set(key, value);
		returnResource(jedis);
		return result;
	}

	public static String get(String key) {
		return get(null, key);
	}

	public static String get(Integer index, String key) {
		Jedis jedis = getResource();
		jedis.select(index == null ? 0 : index);
		String result = jedis.get(key);
		returnResource(jedis);
		return result;
	}

	public static Long hset(String key, String item, String value) {
		return hset(null, key, item, value);
	}

	public static Long hset(Integer index, String key, String item, String value) {
		Jedis jedis = getResource();
		jedis.select(index == null ? 0 : index);
		Long result = jedis.hset(key, item, value);
		returnResource(jedis);
		return result;
	}

	public static String hget(String key, String item) {
		return hget(null, key, item);
	}

	public static String hget(Integer index, String key, String item) {
		Jedis jedis = getResource();
		jedis.select(index == null ? 0 : index);
		String result = jedis.hget(key, item);
		returnResource(jedis);
		return result;
	}

	public static Long incr(String key) {
		return incr(null, key);
	}

	public static Long incr(Integer index, String key) {
		Jedis jedis = getResource();
		jedis.select(index == null ? 0 : index);
		Long result = jedis.incr(key);
		returnResource(jedis);
		return result;
	}

	public static Long decr(String key) {
		return decr(null, key);
	}

	public static Long decr(Integer index, String key) {
		Jedis jedis = getResource();
		jedis.select(index == null ? 0 : index);
		Long result = jedis.decr(key);
		returnResource(jedis);
		return result;
	}

	public static Long expire(String key, int second) {
		return expire(null, key, second);
	}

	public static Long expire(Integer index, String key, int second) {
		Jedis jedis = getResource();
		jedis.select(index == null ? 0 : index);
		Long result = jedis.expire(key, second);
		returnResource(jedis);
		return result;
	}

	public static Long ttl(String key) {
		return ttl(null, key);
	}

	public static Long ttl(Integer index, String key) {
		Jedis jedis = getResource();
		jedis.select(index == null ? 0 : index);
		Long result = jedis.ttl(key);
		returnResource(jedis);
		return result;
	}

	public static Long del(String key) {
		return del(null, key);
	}

	public static Long del(Integer index, String key) {
		Jedis jedis = getResource();
		jedis.select(index == null ? 0 : index);
		Long result = jedis.del(key);
		returnResource(jedis);
		return result;
	}

	public static Long hdel(String key, String item) {
		return hdel(null, key, item);
	}

	public static Long hdel(Integer index, String key, String item) {
		Jedis jedis = getResource();
		jedis.select(index == null ? 0 : index);
		Long result = jedis.hdel(key, item);
		returnResource(jedis);
		return result;
	}

	public static Long hincyby(String key, String item, Long value) {
		return hincyby(null, key, item, value);
	}

	public static Long hincyby(Integer index, String key, String item,
			Long value) {
		Jedis jedis = getResource();
		jedis.select(index == null ? 0 : index);
		Long result = jedis.hincrBy(key, item, value);
		returnResource(jedis);
		return result;
	}

	// 获得公网IP
	public static String getRealIP() {
		try {
			if (realIP == null) {
				String localip = null;// 本地IP，如果没有配置外网IP则返回它
				String netip = null;// 外网IP

				Enumeration<NetworkInterface> netInterfaces = NetworkInterface
						.getNetworkInterfaces();
				InetAddress ip = null;
				boolean finded = false;// 是否找到外网IP
				while (netInterfaces.hasMoreElements() && !finded) {
					NetworkInterface ni = netInterfaces.nextElement();
					Enumeration<InetAddress> address = ni.getInetAddresses();
					while (address.hasMoreElements()) {
						ip = address.nextElement();
						if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress()
								&& ip.getHostAddress().indexOf(":") == -1) {// 外网IP
							netip = ip.getHostAddress();
							finded = true;
							break;
						} else if (ip.isSiteLocalAddress()
								&& !ip.isLoopbackAddress()
								&& ip.getHostAddress().indexOf(":") == -1) {// 内网IP
							localip = ip.getHostAddress();
						}
					}
				}

				if (netip != null && !"".equals(netip)) {
					realIP = netip;
				} else {
					realIP = localip;
				}
			}
			return realIP;
		} catch (Exception e) {
			e.printStackTrace();
			return "unknown host";
		}

	}
}
