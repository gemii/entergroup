package com.jl.nutcloud.core.constant;


/**
 * ClassName:RobotSettingContant.java Function: robot configuration Reason:for robot module
 * @author zili.jin
 * @version
 * @since
 * @Date Mar 20, 2017
 *
 * @see
 */
public class RobotSettingContant {

//	小助手管理中心URL
	public static final String START_ROBOT_URL = "http://localhost:8082/cmd?act=start&bot=BOT&new=NEW";
//	start robot success
	public static final String START_ROBOT_SUCCESS = "0";
//	unknown bot tag
	public static final String UNKOWN_ROBOT_TAG = "1";
	/**
	 * 
	 * @param robotTag 机器人标签
	 * @return
	 * TODO 将机器人标签转为频道 方法
	 */
	public static String tag2Channel(String robotTag){
		return robotTag + "_";
	}
	
}
