package com.jl.nutcloud.core.test;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.jl.nutcloud.core.util.HttpClientUtil;

public class UserControllerTest {

	@Test
	public void phoneCodeTest(){
		String url = "http://localhost:8080/EnterGroup/noauth/user/phoneCode?phoneNumber=18721317074";
		Map<String, String> param = new HashMap<String, String>();
		param.put("phoneNumber", "18721317074");
		System.out.println(HttpClientUtil.doPost(url, param));
	}
	
	@Test
	public void submitTest(){
		Map<String, String> param = new HashMap<String, String>();
		param.put("referralCode", "test");
		param.put("babyName", "babyName");
		param.put("babyBirth", "2017-03-16");
		param.put("babyRelation", "0");
		param.put("phone", "18721317074");
		param.put("email", "zili.jin@gemii.cc");
		param.put("tempID", "@@666666");
		param.put("robotID", "3056368104");
		param.put("openid", "oNPcuvwLFMGdvWTqxKJwKBrEwKV4");
		param.put("privilege", "chinaunicom,chinaunicom");
		param.put("phoneCode", "4192");
		param.put("babyGender", "1");
		System.out.println(HttpClientUtil.doPost("http://localhost:8080/EnterGroup/noauth/user/info", param));
	}
	
	@Test
	public void updateStatusTest(){
		Map<String, String> param = new HashMap<String, String>();
		param.put("id", "9");
		param.put("status", "手工邀请");
		param.put("_method", "PUT");
		System.out.println(HttpClientUtil.doPost("http://localhost:8080/EnterGroup/noauth/admin/enterInfo", param));
	}
}
