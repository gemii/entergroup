package com.jl.nutcloud.core.service;

import java.util.HashMap;
import java.util.Map;

import org.dom4j.DocumentException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.jl.nutcloud.core.AbstractBaseTest;
import com.jl.nutcloud.core.constant.UserSettingConstant;
import com.jl.nutcloud.core.dto.BabyProfile;
import com.jl.nutcloud.core.dto.UserInfoReq;
import com.jl.nutcloud.core.otd.CommonResult;
import com.jl.nutcloud.core.util.HttpClientUtil;
import com.jl.nutcloud.core.util.XMLUtil;

public class UserServiceImplTest extends AbstractBaseTest{
	
	@Autowired
	private IUserService userService;
	
	@Test
	public void submitForm_test(){
		UserInfoReq userInfo = new UserInfoReq();
		userInfo.setCentre("这是一个中心名啊");
		userInfo.setCity("上海");
		userInfo.setCountry("chain");
		userInfo.setEmail("test@gemii.cc");
		userInfo.setPhone("18516513048");
		userInfo.setSex(1);
		BabyProfile babyInfo = new BabyProfile();
		babyInfo.setBabyBirth("2016-04-05");
		babyInfo.setBabyGender(2);
		babyInfo.setBabyName("小宝");
		babyInfo.setBabyRelation("1");
		CommonResult commonResult = userService.submitForm(userInfo, babyInfo);
		System.out.println(JSONObject.toJSONString(commonResult));
	}
	
	@Test
	public void validateVPI_test(String[] args) throws DocumentException {
		Map<String, String> param = new HashMap<String, String>();
		param.put("openid", "test");
		param.put("phone", "18721317074");
		String result = HttpClientUtil.doPost(UserSettingConstant.VALIDATE_VPI_FAN_URL, param);
		System.out.println(result);
		Map<String, Object> resultMap = XMLUtil.xml2map(result);
		Map<String, Object> statusMap = (Map<String, Object>) resultMap.get("status");
		if(UserSettingConstant.VALIDATE_VPI_FAN_SUCCESS.equals(String.valueOf(statusMap.get("status")))){
			System.out.println("asdf");
		}
	}
}
