package com.jl.nutcloud.core.service;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.jl.nutcloud.core.AbstractBaseTest;
import com.jl.nutcloud.core.mapper.AssistantMapper;

public class AssistantServiceImplTest extends AbstractBaseTest{

	@Autowired
	private AssistantMapper assistantMapper;
	
	@Test
	public void addRobot_test(){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("userID", 100);
		param.put("tag", "39238238");
		param.put("role","2");
		assistantMapper.insertClerkRobot(param);
	}
}
