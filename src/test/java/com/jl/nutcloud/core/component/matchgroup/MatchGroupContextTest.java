package com.jl.nutcloud.core.component.matchgroup;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.jl.nutcloud.core.AbstractBaseTest;
import com.jl.nutcloud.core.component.matchGroup.MatchGroupContext;
import com.jl.nutcloud.core.component.matchGroup.MatchGroupContext.MatchGroupStrategy;
import com.jl.nutcloud.core.component.matchGroup.MatchGroupResult;

public class MatchGroupContextTest extends AbstractBaseTest{

	@Autowired
	private MatchGroupContext matchGroupContext;
	
	@Test
	public void matchGroup_test(){
		Map<String,String> conds = new HashMap<String,String>();
		conds.put("centreName","育儿指导中心1");
		MatchGroupResult matchGroupResult = matchGroupContext.matchGroup(MatchGroupStrategy.GYMBOREE_MATCHGROUP_STRATEGY, conds);
		System.out.println(JSONObject.toJSONString(matchGroupResult));
	}
	
}
